package sbu.cs;

import java.util.List;

import javax.lang.model.util.ElementScanner6;

import java.util.ArrayList;
import java.util.Arrays;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
        if(arr.length != 0){
            long sum = 0;
            for (int i = 0 ; i < arr.length ; i++){
                if(i%2 != 0)
                    continue;
                sum += arr[i];
            }
            return sum;
        }
        return 0;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
        if(arr.length != 0){
            int size = arr.length;
            int[] rever = new int[size];
            for (int i = 0 ; i < size ; i++){
                rever[size - i - 1] = arr[i];
            }
            return rever;
        }
        return null;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        if(m1[0].length != m2.length){
            throw new IllegalValueException();
        }
        double[][] mresult = new double[m1.length][m2[0].length]; 
            for (int i = 0; i < m1.length; i++){
                for (int s = 0; s < m2[0].length; s++){
                    for (int j = 0; j < m2.length ; j++){
                        mresult[i][s] += m1[i][j] * m2[j][s];
                    }
                }
            }
            return mresult;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {
        if(names != null){
            List <List <String>> result = new ArrayList <List<String>>();
            for(int i = 0 ; i < names.length ; i++){
                result.add(new ArrayList<String>(Arrays.asList()));
                for(int j = 0 ; j < names[i].length ; j++){
                    result.get(i).add(j,names[i][j]);
                }
            }
            return result;
        }
        return null;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {
        List<Integer> prime = new ArrayList<Integer>();
        if(n > 1){
            if(n % 2 == 0) { 
                prime.add(2);
            } 
            for (int i = 3; i <= Math.sqrt(n); i += 2) { 
                while (n % i == 0) { 
                    prime.add(i); 
                    n /= i; 
                } 
            }
            if(prime.size() == 0){
                prime.add(n);
            } 
            return prime;
        }
        else
            return prime;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        List<String> words = new ArrayList<String>();
        int beg = 0 ;
        for(int i = 0 ; i < line.length() ; i++){
            if(line.charAt(i) <'a' || line.charAt(i) > 'z'){
                String temp = new String(line.substring(beg, i));
                if(temp.length() == 0){
                    beg = i + 1;
                    continue;
                }
                else{
                    words.add(line.substring(beg, i));
                    beg = i + 1;
                    }
                }
            }
        return words;
    }
}
