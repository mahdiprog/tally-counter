package sbu.cs;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n) {
        long factor = 1;
        if (n == 0)
            return 1;
        else if(n > 0){
            for(int count = 1 ; count <= n ; count++){
                factor *= count;
            }
            return factor;
        }
        else{
            return 0;
        }
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public long fibonacci(int n) {
        long f1 = 0 , f2 = 1 , fn = 1;
        if(n == 0){
            return 0;
        }
        else if(n == 1){
            return 1;
        }
        else{
            for(int i = 1 ; i < n ; i++){
                fn = f1 + f2;
                f1 = f2;
                f2 = fn;
            }
            return fn;
        }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word) {
        if(word != null){
            StringBuilder reverse = new StringBuilder();
            for(int i = word.length() - 1 ; i >= 0 ; i--){
                reverse.append(word.charAt(i));
            }
            return reverse.toString();
        }
        else
            return null;
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line) {
        tring sentence = new String(line.replaceAll("\\s+", ""));
        int size = sentence.length();
        for(int i = 0 ; i < size ; i++){
            if(Character.toLowerCase(sentence.charAt(i))  != Character.toLowerCase(sentence.charAt(size - i - 1)))
                return false;
        }
        return true;
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2) {
        if(str1 != null || str2 != null){
            char[][] dots = new char[str1.length()][str2.length()];
            for (int i = 0; i < str1.length(); i++) {
                for (int j = 0; j < str2.length(); j++) {
                    dots[i][j] = ' ';
                }
            }
            for(int i = 0 ; i < str1.length() ; i++){
                for(int j = 0 ; j < str2.length() ; j++){
                    if(str1.charAt(i) == str2.charAt(j))
                        dots[i][j] = '*';
                }
            }
        return dots;
        }
        return null;
    }
}