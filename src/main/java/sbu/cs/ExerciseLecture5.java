package sbu.cs;

import org.graalvm.compiler.graph.spi.Canonicalizable.Binary;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) {
        if(length != 0){
            StringBuilder password = new StringBuilder();
            for(int i = 0 ; i < length ; i++){
                int num = (int)(Math.random() * (122 - 97 + 1) + 97 ) ;
                char ch = (char)(num);
                password.append(ch);  
            }
            return password.toString();
        }
        return null;
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {
        if(length < 3){
            throw new Exception();
        }
        else{
            StringBuilder temp = new StringBuilder();
            boolean hasDigit = false , hasCharacter = false;
            for(int i = 0 ; i < length - 2; i++){
                int num = (int)(Math.random() * (122 - 97 + 1) + 97 ) ;
                char ch = (char)(num);
                temp.append(ch);  
            }
            char ch = (char)((Math.random() * (47 - 33 + 1) + 33)); 
            int cwhere = (int)(Math.random() * (length - 1 - 0 + 1));
            char digit = (char)((Math.random() * (57 - 48 + 1) + 48));
            int dwhere;
            do{
                dwhere = (int)(Math.random() * (length - 1 - 0 + 1));
            }while(dwhere == cwhere);
            StringBuilder password = new StringBuilder();
            int low = 0;
            for(int i = 0 ; i < length; i++){
                if(cwhere == i && !hasCharacter){
                    password.append(ch);
                    hasCharacter = true;
                    low++;
                    continue;
                }
                if(dwhere == i && !hasDigit){
                    password.append(digit);
                    hasDigit = true;
                    low++;
                    continue;
                }
                password.append(temp.charAt(i - low));  
            }
            return password.toString();
        }
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n) {
        int fib = 0 , f1 = 0 , f2 = 1;
        while(fib < n){
            fib = f1 + f2;
            f1 = f2;
            f2 = fib;
            String bin = Integer.toBinaryString(fib);
            int countOne = 0;
            for(int i = 0 ; i < bin.length() ; i++){
                if(bin.charAt(i) == '1'){
                    countOne++;
                }
            }
            if(fib + countOne == n){
                return true;
            }
        }
        return false;
    }
}